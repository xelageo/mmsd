#ifndef __RESOLV_COMPAT
#define __RESOLV_COMPAT
/*
 * Definitions of res_nclose and res_ninit, for other libc (e.g.
 * musl)
 *
 * res_init is apparently deprecated according to this, but still in musl
 * https://man7.org/linux/man-pages/man3/resolver.3.html
 */
#if !defined(__GLIBC__)
#include <resolv.h>

static inline int res_ninit (res_state statep) {
	/* res_init is a stub in musl... */
	return res_init();
}

static inline int res_nclose (res_state statep) {
	/* nothing was allocated in the res_ninit stub */
	return 0;
}
#endif  /* ! __GLIBC__ */
#endif /* __RESOLV_COMPAT */
